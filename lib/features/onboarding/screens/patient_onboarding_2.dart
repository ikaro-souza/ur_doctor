import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/features/onboarding/screens/screens.dart';
import 'package:ur_doctor/widgets/elevated_button.dart';

class PatientOnboarding2 extends StatelessWidget {
  static const String route = '/onboarding/patient/2';

  const PatientOnboarding2({Key? key}) : super(key: key);

  double get _horizontalPadding => 48;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 0, 0),
                  child: IconButton(
                    color: Palette.primary,
                    icon: const Icon(Icons.arrow_back_ios),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                    _horizontalPadding,
                    8,
                    _horizontalPadding,
                    0,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: _buildTopSection(context),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 0, 24, 32),
              child: Row(
                children: [
                  TextButton(
                    child: const Text('Pular'),
                    onPressed: () => Navigator.of(context).pushNamed(
                      PatientSignUpDisplayName.route,
                    ),
                  ),
                  const SizedBox(width: 48),
                  Expanded(
                    child: SizedBox(
                      height: 64,
                      child: MyElevatedButton(
                        label: 'Próximo',
                        icon: const Icon(Icons.arrow_forward),
                        onPressed: () => Navigator.of(context).pushNamed(
                          PatientOnboarding3.route,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildTopSection(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final imageHeight = screenSize.height / 3;

    return [
      SizedBox(
        height: imageHeight,
        width: screenSize.width - _horizontalPadding,
        child: Hero(
          tag: 'onboarding_image',
          child: SvgPicture.asset(
            'assets/images/staff.svg',
            fit: BoxFit.cover,
          ),
        ),
      ),
      const SizedBox(height: 24),
      Hero(
        tag: 'onboarding_title',
        child: Text(
          'Marque suas consultas',
          style: _getTitleStyle(context),
          textAlign: TextAlign.center,
        ),
      ),
      const SizedBox(height: 16),
      Hero(
        tag: 'onboarding_description',
        child: Text(
          'Marque suas consultas com facilidade e agilidade.',
          style: _getDescriptionStyles(context),
          textAlign: TextAlign.center,
        ),
      ),
    ];
  }

  TextStyle _getTitleStyle(BuildContext context) {
    return Theme.of(context).textTheme.headline2!;
  }

  TextStyle _getDescriptionStyles(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2!;
  }
}
