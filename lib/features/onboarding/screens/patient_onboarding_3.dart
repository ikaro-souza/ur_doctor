import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientOnboarding3 extends StatelessWidget {
  static const String route = '/onboarding/patient/3';

  const PatientOnboarding3({Key? key}) : super(key: key);

  double get _horizontalPadding => 48;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 0, 0),
                  child: IconButton(
                    color: Palette.primary,
                    icon: const Icon(Icons.arrow_back_ios),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(
                    _horizontalPadding,
                    8,
                    _horizontalPadding,
                    0,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: _buildTopSection(context),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 0, 24, 32),
              child: Row(
                children: [
                  TextButton(
                    child: const Text('Pular'),
                    onPressed: () {},
                  ),
                  const SizedBox(width: 48),
                  Expanded(
                    child: SizedBox(
                      height: 64,
                      child: MyElevatedButton(
                        label: 'Próximo',
                        icon: const Icon(Icons.arrow_forward),
                        onPressed: () => Navigator.of(context).pushNamed(
                          PatientSignUpDisplayName.route,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildTopSection(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final imageHeight = screenSize.height / 3;

    return [
      SizedBox(
        height: imageHeight,
        width: screenSize.width - _horizontalPadding,
        child: Hero(
          tag: 'onboarding_image',
          child: SvgPicture.asset(
            'assets/images/doctor.svg',
            fit: BoxFit.cover,
          ),
        ),
      ),
      const SizedBox(height: 24),
      Hero(
        tag: 'onboarding_title',
        child: Text(
          'Consulte-se \nde casa',
          style: _getTitleStyle(context),
          textAlign: TextAlign.center,
        ),
      ),
      const SizedBox(height: 16),
      Hero(
        tag: 'onboarding_description',
        child: Text(
          'Seja atendido pelo seu médico no conforto da sua casa.',
          style: _getDescriptionStyles(context),
          textAlign: TextAlign.center,
        ),
      ),
    ];
  }

  TextStyle _getTitleStyle(BuildContext context) {
    return Theme.of(context).textTheme.headline2!;
  }

  TextStyle _getDescriptionStyles(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2!;
  }
}
