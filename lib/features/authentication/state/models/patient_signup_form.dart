class PatientSignupForm {
  final String displayName;
  final String fullName;
  final PatientSex sex;
  final String taxId;
  final DateTime birthDate;
  final String token;
  final String email;

  PatientSignupForm({
    required this.displayName,
    required this.fullName,
    required this.sex,
    required this.taxId,
    required this.birthDate,
    required this.token,
    required this.email,
  });
}

enum PatientSex { masc, fem }

extension PatientSexExtensions on PatientSex {
  String get displayText {
    return this == PatientSex.masc ? 'Masculino' : 'Feminino';
  }
}

extension StringExtensions on String {
  PatientSex get toPatientSex {
    return this == 'Masculino' ? PatientSex.masc : PatientSex.fem;
  }
}
