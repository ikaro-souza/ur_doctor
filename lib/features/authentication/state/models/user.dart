class User {
  final int id;
  final String name;
  final String taxId;
  final UserType type;
  final String phone;
  final String email;

  // doctor fields
  String? license;

  // patient fields
  String? displayName;

  User({
    required this.id,
    required this.name,
    required this.type,
    required this.taxId,
    required this.phone,
    required this.email,
    String? license,
    String? displayName,
  }) {
    final isDoctor = type == UserType.doctor;

    assert((isDoctor && license != null) || (!isDoctor && license == null));

    if (isDoctor) {
      this.license = license!;
    } else {
      this.displayName = displayName!;
    }
  }

  factory User.patient({
    required int id,
    required String name,
    required String taxId,
    required String phone,
    required String email,
    required String displayName,
  }) {
    return User(
      id: id,
      name: name,
      type: UserType.patient,
      taxId: taxId,
      phone: phone,
      email: email,
      displayName: displayName,
    );
  }

  factory User.doctor({
    required int id,
    required String name,
    required String taxId,
    required String license,
    required String phone,
    required String email,
  }) {
    return User(
      id: id,
      name: name,
      type: UserType.doctor,
      taxId: taxId,
      license: license,
      phone: phone,
      email: email,
    );
  }

  @override
  // ignore: hash_and_equals
  bool operator ==(Object other) {
    return id == (other as User).id;
  }
}

enum UserType { patient, doctor }
