import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpEmail extends StatelessWidget {
  static const String route = 'auth/signup/patient/email';

  const PatientSignUpEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(context),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpCreatePassword.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ana, qual o seu\ne-mail?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Insira o seu e-mail.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: TextField(
        autofocus: true,
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          fillColor: Palette.surfaceVariant,
          filled: true,
          border: InputBorder.none,
          labelText: 'E-mail',
        ),
        inputFormatters: [
          LengthLimitingTextInputFormatter(50),
        ],
      ),
    );
  }
}
