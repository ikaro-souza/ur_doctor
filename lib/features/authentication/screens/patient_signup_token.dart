import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpToken extends StatelessWidget {
  static const String route = 'auth/signup/patient/token';

  const PatientSignUpToken({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(context),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpEmail.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ana, você vai receber um token.', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Insira o token recebido via SMS ou WhatsApp.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    const tokenLength = 6;
    const margin = 8.0;

    return OtpTextField(
      focusedBorderColor: Palette.primary,
      borderColor: Palette.outline,
      autoFocus: true,
      numberOfFields: tokenLength,
      margin: const EdgeInsets.only(right: margin),
      borderRadius: BorderRadius.circular(12),
      textStyle: Theme.of(context).textTheme.headline3!,
      fieldWidth:
          (MediaQuery.of(context).size.width / tokenLength) - 2 * margin,
    );
  }
}
