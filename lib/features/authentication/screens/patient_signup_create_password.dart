import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpCreatePassword extends StatefulWidget {
  static const String route = 'auth/signup/patient/password';

  const PatientSignUpCreatePassword({Key? key}) : super(key: key);

  @override
  _PatientSignUpCreatePasswordState createState() =>
      _PatientSignUpCreatePasswordState();
}

class _PatientSignUpCreatePasswordState
    extends State<PatientSignUpCreatePassword> {
  final _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(context),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpConfirmPassword.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ana, por favor crie sua senha.', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Crie uma senha segura para acessar sua conta.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: TextField(
        controller: _textController,
        autofocus: true,
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        enableSuggestions: false,
        autocorrect: false,
        decoration: const InputDecoration(
          fillColor: Palette.surfaceVariant,
          filled: true,
          border: InputBorder.none,
          labelText: 'Senha',
        ),
        inputFormatters: [
          LengthLimitingTextInputFormatter(43),
        ],
      ),
    );
  }
}
