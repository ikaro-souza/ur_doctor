import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class RecoverPassword extends StatelessWidget {
  static const String route = "/auth/recover-password";

  const RecoverPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Recuperar senha',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _buildTopSection(context),
            _buildBottomSection(context),
          ],
        ),
      ),
    );
  }

  Widget _buildTopSection(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildTitle(context),
        const SizedBox(height: 56),
        _buildForm(context),
      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Recupere sua senha',
          style: textTheme.headline1,
        ),
        const SizedBox(height: 16),
        Text(
          'Informe seu e-mail para recuperar sua senha.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    return Container(
      height: 56,
      decoration: BoxDecoration(
        color: Palette.surfaceVariant,
        borderRadius: BorderRadius.circular(16),
      ),
    );
  }

  Widget _buildBottomSection(BuildContext context) {
    return const MyElevatedButton(
      label: 'Confirmar',
      height: 64,
    );
  }
}
