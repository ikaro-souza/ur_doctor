import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpBirthDate extends StatefulWidget {
  static const String route = 'auth/signup/patient/birthdate';

  const PatientSignUpBirthDate({Key? key}) : super(key: key);

  @override
  State<PatientSignUpBirthDate> createState() => _PatientSignUpBirthDateState();
}

class _PatientSignUpBirthDateState extends State<PatientSignUpBirthDate> {
  final _textController = TextEditingController();

  DateTime? _birthDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpPhoneNumber.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ana, quando você nasceu?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Insira a sua data de nascimento.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: TextField(
        autofocus: true,
        readOnly: true,
        controller: _textController,
        decoration: const InputDecoration(
          fillColor: Palette.surfaceVariant,
          filled: true,
          border: InputBorder.none,
          labelText: 'Data de nascimento',
        ),
        onTap: () {
          showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(1921),
            lastDate: DateTime.now(),
          ).then((value) {
            if (value == null) return;

            setState(() {
              _birthDate = value;
              _textController.text = value.toIso8601String();
            });
          });
        },
      ),
    );
  }
}
