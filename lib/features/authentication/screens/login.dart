import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/features/patient/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class Login extends StatefulWidget {
  static const String route = 'auth/login';

  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _taxIdController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _rememberMe = false;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final bottomInsets = mediaQuery.viewInsets.bottom;
    final spacing = bottomInsets > 0
        ? const SizedBox(height: 40)
        : Expanded(child: Container());
    const appBar = MyAppBar(
      title: 'Entrar',
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: mediaQuery.size.height -
                bottomInsets -
                appBar.preferredSize.height -
                32,
          ),
          child: IntrinsicHeight(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _buildTopSection(),
                  spacing,
                  _buildBottomSection(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTopSection() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildTitle(),
        const SizedBox(height: 56),
        _buildForm(),
      ],
    );
  }

  Widget _buildTitle() {
    final textStyle = Theme.of(context).textTheme.headline1;

    // TODO: Get user gender from SharedPreferences

    return Text(
      'Bem-vinda Ana!',
      style: textStyle,
    );
  }

  Widget _buildForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: TextField(
            controller: _taxIdController,
            autofocus: true,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            decoration: const InputDecoration(
              fillColor: Palette.surfaceVariant,
              filled: true,
              border: InputBorder.none,
              labelText: 'CPF',
            ),
            inputFormatters: [
              MaskTextInputFormatter(
                mask: '###.###.###-##',
                filter: {
                  '#': RegExp(r'[0-9]'),
                },
              ),
            ],
          ),
        ),
        const SizedBox(height: 16),
        ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: TextField(
            controller: _passwordController,
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            decoration: const InputDecoration(
              fillColor: Palette.surfaceVariant,
              filled: true,
              border: InputBorder.none,
              labelText: 'Senha',
            ),
            inputFormatters: [
              LengthLimitingTextInputFormatter(43),
            ],
          ),
        ),
        const SizedBox(height: 16),
        CheckBox(
          label: 'Lembrar de mim',
          checked: _rememberMe,
          onChange: _onRememberMeTap,
        ),
      ],
    );
  }

  Widget _buildBottomSection() {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        MyElevatedButton(
          label: 'Entrar',
          height: 64,
          onPressed: () => Navigator.of(context).pushReplacementNamed(
            PatientHome.route,
          ),
        ),
        const SizedBox(height: 16),
        GestureDetector(
          onTap: _onForgotPasswordTap,
          child: Text(
            'Esqueceu sua senha?',
            style: textTheme.bodyText2!.copyWith(fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }

  void _onRememberMeTap(bool value) {
    setState(() {
      _rememberMe = value;
    });
  }

  void _onForgotPasswordTap() =>
      Navigator.of(context).pushNamed(RecoverPassword.route);
}
