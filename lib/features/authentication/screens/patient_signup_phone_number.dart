import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpPhoneNumber extends StatefulWidget {
  static const String route = 'auth/signup/patient/phone';

  const PatientSignUpPhoneNumber({Key? key}) : super(key: key);

  @override
  State<PatientSignUpPhoneNumber> createState() =>
      _PatientSignUpPhoneNumberState();
}

class _PatientSignUpPhoneNumberState extends State<PatientSignUpPhoneNumber> {
  final _textController = TextEditingController();
  bool isWhatsApp = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(context),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpToken.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ana, qual seu telefone?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Insira o seu número de telefone.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: TextField(
            autofocus: true,
            keyboardType: TextInputType.phone,
            controller: _textController,
            decoration: const InputDecoration(
              fillColor: Palette.surfaceVariant,
              filled: true,
              border: InputBorder.none,
              labelText: 'Telefone',
            ),
            inputFormatters: [
              MaskTextInputFormatter(
                mask: '(##) 9 ####-####',
                filter: {
                  '#': RegExp(r'[0-9]'),
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
