import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpCompleted extends StatelessWidget {
  static const String route = 'auth/signup/patient/end';

  const PatientSignUpCompleted({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 3,
                  child: SvgPicture.asset(
                    'assets/images/signup_completed.svg',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                const SizedBox(height: 24),
                Text(
                  'Pronto Ana! Você já pode acessar sua conta.',
                  style: Theme.of(context).textTheme.headline3,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            MyElevatedButton(
              label: 'Acessar',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () async {
                final navigator = Navigator.of(context);
                navigator.popUntil((route) => route.isFirst);
                navigator.pushReplacementNamed(Login.route);
              },
            )
          ],
        ),
      ),
    );
  }
}
