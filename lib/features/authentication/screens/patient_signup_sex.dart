import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/features/authentication/state/models/patient_signup_form.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientSignUpSex extends StatefulWidget {
  static const String route = 'auth/signup/patient/sex';

  const PatientSignUpSex({Key? key}) : super(key: key);

  @override
  State<PatientSignUpSex> createState() => _PatientSignUpSexState();
}

class _PatientSignUpSexState extends State<PatientSignUpSex> {
  final _items = [
    const DropdownMenuItem(
      child: Text('Sexo'),
      value: null,
      enabled: false,
    ),
    ...PatientSex.values.map(
      (e) => DropdownMenuItem(child: Text(e.displayText), value: e),
    )
  ];

  PatientSex? _sex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Cadastro',
        showBackButton: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildTitleAndDescription(context),
                const SizedBox(height: 56),
                _buildForm(context),
              ],
            ),
            MyElevatedButton(
              label: 'Próximo',
              height: 64,
              borderRadius: 16,
              icon: const Icon(Icons.arrow_forward),
              onPressed: () => Navigator.of(context).pushNamed(
                PatientSignUpTaxId.route,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ok Ana. Qual o seu sexo?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Escolha o seu sexo.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: DropdownButtonFormField<PatientSex?>(
        hint: const Text('Sexo'),
        value: _sex,
        isExpanded: true,
        itemHeight: 56,
        items: _items.toList(),
        decoration: const InputDecoration(
          filled: true,
          fillColor: Palette.surfaceVariant,
          border: InputBorder.none,
        ),
        onChanged: (value) {
          setState(() {
            _sex = (value as String).toPatientSex;
          });
        },
      ),
    );
  }
}
