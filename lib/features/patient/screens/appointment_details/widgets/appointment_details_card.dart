import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class AppointmentDetailsCard extends StatelessWidget {
  const AppointmentDetailsCard({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  EdgeInsets get _sectionPadding => const EdgeInsets.all(16);

  @override
  Widget build(BuildContext context) {
    final appointmentSection = _appointmentSection(context, appointment);
    final patientSection = _patientSection(context);

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 48),
      decoration: BoxDecoration(
        boxShadow: kHighElevation,
        color: Palette.surface,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(padding: _sectionPadding, child: appointmentSection),
          const Divider(height: 0),
          Padding(padding: _sectionPadding, child: patientSection),
        ],
      ),
    );
  }

  Widget _appointmentSection(BuildContext context, Appointment appointment) {
    final tiles = [
      _informationTile(context, 'Assunto', appointment.title),
      _informationTile(
        context,
        'Descrição',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mattis dolor feugiat non tempor.',
      ),
      _informationTile(context, 'Data', appointment.date.toString())
    ];

    return _section(
      context: context,
      title: 'Consulta',
      tiles: tiles,
    );
  }

  Widget _patientSection(BuildContext context) {
    // TODO: Get patient data from state
    final tiles = [
      _informationTile(context, 'Nome completo', 'Ana Vitória Alves da Silva'),
      _informationTile(context, 'Idade', '20 anos'),
      _informationTile(context, 'Sexo', 'Feminino'),
      _informationTile(
          context,
          'Sintomas',
          [
            'Náusea',
            'Indisposição',
            'Falta de ar',
          ].join(', ')),
    ];

    return _section(
      context: context,
      title: 'Paciente',
      tiles: tiles,
    );
  }

  Widget _section({
    required BuildContext context,
    required String title,
    required List<Widget> tiles,
  }) {
    final spacedTiles = tiles.map((e) {
      final isLastTile = tiles.indexOf(e) == tiles.length - 1;
      return isLastTile
          ? e
          : Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: e,
            );
    });

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _sectionTitle(context, title),
        const SizedBox(height: 16),
        ...spacedTiles,
      ],
    );
  }

  Widget _sectionTitle(BuildContext context, String title) {
    final textStyle = Theme.of(context).textTheme.headline6;
    return Text(title, style: textStyle);
  }

  Widget _informationTile(BuildContext context, String name, String value) {
    final textTheme = Theme.of(context).textTheme;
    final nameTextStyle = textTheme.caption;
    final valueTextStyle = textTheme.bodyText2!.copyWith(
      fontWeight: FontWeight.w500,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name,
          style: nameTextStyle,
        ),
        const SizedBox(height: 4),
        Text(value, style: valueTextStyle),
      ],
    );
  }
}
