import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/patient/screens/appointment_details/widgets/widgets.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientAppointmentDetails extends StatelessWidget {
  static const String route = 'patient/appointments/details';

  const PatientAppointmentDetails({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    final _appointment = appointment;

    return Scaffold(
      appBar: const MyAppBar(
        showBackButton: true,
        title: 'Consulta',
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 48, horizontal: 24),
          child: AppointmentDetailsCard(appointment: _appointment),
        ),
      ),
      floatingActionButton: SizedBox(
        width: MediaQuery.of(context).size.width - 48,
        child: FloatingActionButton.extended(
          label: const Text('Iniciar'),
          icon: const Icon(Icons.video_call),
          onPressed: () => Navigator.of(context).pushNamed(
            AppointmentCall.route,
            arguments: appointment,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
