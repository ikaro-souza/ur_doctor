import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/patient/screens/screens.dart';
import 'package:ur_doctor/features/patient/screens/home/widgets/widgets.dart';

class PatientHome extends StatefulWidget {
  static const String route = 'patient/';

  const PatientHome({Key? key}) : super(key: key);

  @override
  State<PatientHome> createState() => _PatientHomeState();
}

class _PatientHomeState extends State<PatientHome> {
  final _pageController = PageController();

  int _currentPage = 0;
  int _previousPage = 0;

  @override
  Widget build(BuildContext context) {
    final _pages = [
      HomeView(onListTileTap: _onBottomNavigationBarTap),
      PatientAppointments(onBackButtonTap: _onPageBackButtonTap),
      Messages(onBackButtonTap: _onPageBackButtonTap),
    ];

    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _pages,
        physics: const NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentPage,
        onTap: _onBottomNavigationBarTap,
        items: const [
          BottomNavigationBarItem(
            label: 'Início',
            icon: Icon(Icons.home_outlined),
            activeIcon: Icon(Icons.home_rounded),
          ),
          BottomNavigationBarItem(
            label: 'Consultas',
            icon: Icon(Icons.calendar_today_outlined),
            activeIcon: Icon(Icons.calendar_today),
          ),
          BottomNavigationBarItem(
            label: 'Mensagens',
            icon: Icon(Icons.message_outlined),
            activeIcon: Icon(Icons.message),
          ),
        ],
      ),
    );
  }

  void _onBottomNavigationBarTap(int index) {
    setState(() {
      _previousPage = _currentPage;
      _currentPage = index;
    });
    _pageController.animateToPage(
      index,
      duration: kPageTransitionDuration,
      curve: kPageTransitionCurve,
    );
  }

  void _onPageBackButtonTap() {
    setState(() {
      _currentPage = _previousPage;
    });
    _pageController.animateToPage(
      _previousPage,
      duration: kAnimationDuration,
      curve: kAnimationCurve,
    );
  }
}
