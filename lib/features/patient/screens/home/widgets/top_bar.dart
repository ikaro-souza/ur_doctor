import 'package:flutter/material.dart';
import 'package:ur_doctor/features/patient/screens/home/widgets/widgets.dart';
import 'package:ur_doctor/features/patient/widgets/widgets.dart';

class TopBar extends StatelessWidget {
  const TopBar({Key? key, required this.onListTileTap}) : super(key: key);

  final Function(int) onListTileTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onTap(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          _buildUserGreeting(context),
          const SizedBox(width: 16),
          const UserImage()
        ],
      ),
    );
  }

  Widget _buildUserGreeting(BuildContext context) {
    // TODO: Get user display name from state
    const displayName = 'Ana';
    final textStyle = Theme.of(context).textTheme.subtitle1!.copyWith(
          fontWeight: FontWeight.w500,
        );

    return Text(
      'Bom dia, $displayName',
      style: textStyle,
    );
  }

  void _onTap(BuildContext context) {
    const borderRadius = BorderRadius.vertical(top: Radius.circular(24));
    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: borderRadius,
      ),
      constraints: BoxConstraints(
        maxHeight: MediaQuery.of(context).size.height / 2,
      ),
      builder: (_) => ClipRRect(
        borderRadius: borderRadius,
        child: MainBottomSheet(onListTileTap: onListTileTap),
      ),
    );
  }
}
