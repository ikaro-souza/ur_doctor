import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class SearchSection extends StatefulWidget {
  const SearchSection({Key? key}) : super(key: key);

  @override
  State<SearchSection> createState() => _SearchSectionState();
}

class _SearchSectionState extends State<SearchSection> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return SizedBox(
      height: screenHeight / 3,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: _buildImage(),
          ),
          Positioned(
            bottom: 24,
            left: 0,
            right: 0,
            child: _buildSearchCard(),
          )
        ],
      ),
    );
  }

  Widget _buildImage() {
    return SvgPicture.asset(
      'assets/images/staff.svg',
      fit: BoxFit.fitHeight,
    );
  }

  Widget _buildSearchCard() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Palette.surface,
        borderRadius: BorderRadius.circular(16),
        boxShadow: kHighElevation,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 48,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Palette.surfaceVariant,
              borderRadius: BorderRadius.circular(16),
            ),
          ),
          const SizedBox(height: 16),
          Row(
            children: const [
              Chip(label: Text('Visão')),
              SizedBox(width: 12),
              Chip(label: Text('Angiografia')),
              SizedBox(width: 12),
              Chip(label: Text('Ansiedade')),
            ],
          ),
        ],
      ),
    );
  }
}
