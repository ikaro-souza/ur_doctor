import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/patient/screens/home/widgets/widgets.dart';
import 'package:ur_doctor/features/patient/state/models/models.dart';
import 'package:ur_doctor/features/patient/widgets/appointment_card.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key, required this.onListTileTap}) : super(key: key);

  final Function(int) onListTileTap;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          TopBar(onListTileTap: onListTileTap),
          const SizedBox(height: 24),
          const SearchSection(),
          const SizedBox(height: 40),
          const SectionTitle(text: 'Próxima consulta'),
          const SizedBox(height: 12),
          AppointmentCard(
            appointment: dummyAppointments.last,
          )
        ],
      ),
    );
  }
}
