import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';

class UserImage extends StatelessWidget {
  const UserImage({Key? key, this.size = 56}) : super(key: key);

  final double size;

  @override
  Widget build(BuildContext context) {
    // TODO: Get user image from state
    const imageUrl =
        'https://images.unsplash.com/photo-1521227889351-bf6f5b2e4e37?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1025&q=80';

    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        border: Border.all(color: Palette.primaryContainer, width: 2),
        borderRadius: BorderRadius.circular(size),
      ),
      child: CircleAvatar(
        radius: size - 2,
        backgroundColor: Palette.primaryContainer,
        foregroundImage: const NetworkImage(imageUrl),
        child: const Icon(
          Icons.account_circle_outlined,
          color: Palette.primary,
        ),
      ),
    );
  }
}
