import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/features/patient/screens/appointments/widgets/widgets.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class PatientAppointments extends StatelessWidget {
  static const String route = 'patient/appointments';

  const PatientAppointments({
    Key? key,
    required this.onBackButtonTap,
  }) : super(key: key);

  final VoidCallback onBackButtonTap;

  EdgeInsets get _pagePadding => const EdgeInsets.symmetric(
        vertical: 32,
        horizontal: 16,
      );

  @override
  Widget build(BuildContext context) {
    final appointments = List<Appointment>.from(dummyAppointments);

    return Scaffold(
      appBar: FlatAppBar(
        title: 'Consultas',
        onBackButtonTap: onBackButtonTap,
      ),
      body: appointments.isNotEmpty
          ? _buildAppointmentsLayout(appointments)
          : _buildEmptyAppointmentsLayout(),
      floatingActionButton: MyIconButton(
        icon: Icons.add_circle,
        boxShadow: kLowElevation,
        onTap: () => Navigator.of(context).pushNamed(
          ScheduleAppointmentDate.route,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _buildAppointmentsLayout(List<Appointment> appointments) {
    return Appointments(
      appointments: appointments,
      pagePadding: _pagePadding,
    );
  }

  Widget _buildEmptyAppointmentsLayout() {
    return Padding(
      padding: _pagePadding,
      child: const NoAppointments(),
    );
  }
}
