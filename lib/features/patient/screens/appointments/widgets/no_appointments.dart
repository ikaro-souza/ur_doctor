import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class NoAppointments extends StatelessWidget {
  const NoAppointments({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      children: [
        SvgPicture.asset(
          'assets/images/empty.svg',
          height: MediaQuery.of(context).size.height / 3,
          fit: BoxFit.fitHeight,
        ),
        const SizedBox(height: 24),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildTitle(textTheme),
            const SizedBox(height: 4),
            _buildDescription(textTheme),
          ],
        ),
      ],
    );
  }

  Widget _buildTitle(TextTheme textTheme) {
    final textStyle = textTheme.headline2!.copyWith(
      fontWeight: FontWeight.bold,
    );

    return Text(
      'Nenhuma consulta agendada',
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }

  Widget _buildDescription(TextTheme textTheme) {
    final textStyle = textTheme.bodyText1!;

    return Text(
      'Agende uma consulta apertando o botão abaixo.',
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }
}
