import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/features/patient/widgets/appointment_card.dart';

class Appointments extends StatelessWidget {
  const Appointments({
    Key? key,
    required this.appointments,
    required this.pagePadding,
  }) : super(key: key);

  final List<Appointment> appointments;
  final EdgeInsets pagePadding;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      primary: false,
      padding: pagePadding,
      itemBuilder: (_, i) {
        final appointment = appointments[i];
        return AppointmentCard(
          appointment: appointment,
        );
      },
      separatorBuilder: (_, __) => const SizedBox(height: 24),
      itemCount: appointments.length,
    );
  }
}
