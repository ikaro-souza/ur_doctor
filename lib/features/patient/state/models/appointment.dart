class Appointment {
  final int id;
  final String title;
  final DateTime date;

  const Appointment({
    required this.id,
    required this.title,
    required this.date,
  });
}
