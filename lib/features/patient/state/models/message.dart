import 'package:ur_doctor/features/patient/state/models/models.dart';

class Message {
  final int id;
  final String content;
  final Appointment appointment;

  const Message({
    required this.id,
    required this.content,
    required this.appointment,
  });
}
