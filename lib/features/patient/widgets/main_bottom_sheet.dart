import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class MainBottomSheet extends StatefulWidget {
  const MainBottomSheet({Key? key, required this.onListTileTap})
      : super(key: key);

  final Function(int) onListTileTap;

  @override
  State<MainBottomSheet> createState() => _MainBottomSheetState();
}

class _MainBottomSheetState extends State<MainBottomSheet> {
  int _selectedItem = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.surface1,
      padding: const EdgeInsets.all(12),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListItem(
            label: 'Início',
            leading: const Icon(Icons.home),
            selected: _selectedItem == 0,
            onTap: () => _onItemTap(0),
          ),
          ListItem(
            label: 'Consultas',
            leading: const Icon(Icons.calendar_today),
            selected: _selectedItem == 1,
            onTap: () => _onItemTap(1),
          ),
          ListItem(
            label: 'Mensagens',
            leading: const Icon(Icons.message),
            selected: _selectedItem == 2,
            onTap: () => _onItemTap(2),
          ),
          const Divider(height: 36),
          ListItem(
            label: 'Sua conta',
            leading: const Icon(Icons.account_circle),
            selected: _selectedItem == 3,
            onTap: () => _onItemTap(3),
          ),
        ],
      ),
    );
  }

  void _onItemTap(int index) {
    setState(() {
      _selectedItem = index;
    });

    widget.onListTileTap(index);
    Navigator.of(context).pop();
  }
}
