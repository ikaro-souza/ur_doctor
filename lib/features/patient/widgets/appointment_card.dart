import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/patient/screens/screens.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class AppointmentCard extends StatelessWidget {
  const AppointmentCard({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        boxShadow: kHighElevation,
        borderRadius: BorderRadius.circular(16),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Material(
          color: Palette.surface,
          child: InkWell(
            onTap: () => Navigator.of(context).pushNamed(
              PatientAppointmentDetails.route,
              arguments: appointment,
            ),
            child: _content(context),
          ),
        ),
      ),
    );
  }

  Widget _content(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Chip(label: Text('20 Nov')),
          const SizedBox(height: 4),
          _titleRow(context),
        ],
      ),
    );
  }

  Widget _titleRow(BuildContext context) {
    final titleTextStyle = Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.bold,
        );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          appointment.title,
          style: titleTextStyle,
          overflow: TextOverflow.ellipsis,
        ),
        Container(
          height: 48,
          width: 48,
          decoration: BoxDecoration(
            color: Palette.primaryContainer,
            borderRadius: BorderRadius.circular(12),
          ),
          child: IconButton(
            icon: const Icon(
              Icons.message,
              color: Palette.primary,
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
