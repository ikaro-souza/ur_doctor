import 'package:intl/intl.dart';

String formatAppointmentSchedulingDate(DateTime date) {
  return DateFormat('dd \'d\'e MMMM yyyy HH:mm', 'pt_BR').format(date);
}

String formatSymptoms(List<String> symptoms) {
  final first =
      '${symptoms.first.substring(0, 1).toUpperCase()}${symptoms.first.substring(1)}';
  final last = symptoms.length > 1 ? symptoms.last.toLowerCase() : null;
  final rest = symptoms
      .sublist(1, symptoms.length - 1)
      .map((e) => e.toLowerCase())
      .join(', ');

  var result = first;
  if (rest.isNotEmpty) {
    result = '$result, $rest';
  }
  if (last != null) {
    result = '$result e $last';
  }

  return result;
}

String formatSymptomsStartDate(DateTime time) {
  return DateFormat('dd \'d\'e MMMM yyyy', 'pt_BR').format(time);
}
