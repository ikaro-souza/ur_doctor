import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/features/common/state/models/appointment.dart';
import 'package:ur_doctor/features/common/utils/extensions.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class CallRatingCard extends StatelessWidget {
  const CallRatingCard({Key? key, required this.appointment}) : super(key: key);

  final Appointment appointment;

  double get _cardPadding => 24;

  EdgeInsets get _sectionPadding => const EdgeInsets.symmetric(horizontal: 16);

  Widget get _sectionDevider => const Opacity(
        opacity: .5,
        child: Divider(height: 48),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        boxShadow: kHighElevation,
        color: Palette.surface,
      ),
      padding: EdgeInsets.symmetric(vertical: _cardPadding),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: _sectionPadding,
            child: _topSection(context),
          ),
          _sectionDevider,
          Padding(
            padding: _sectionPadding,
            child: _summarySection(context),
          ),
          _sectionDevider,
          Padding(
            padding: _sectionPadding,
            child: _ratingSection(context),
          ),
        ],
      ),
    );
  }

  Widget _topSection(BuildContext context) {
    // TODO: Get doctor and patient information from state

    final textTheme = Theme.of(context).textTheme;
    final nameTextStyle = textTheme.caption;
    final valueTextStyle = textTheme.bodyText2!.copyWith(
      fontWeight: FontWeight.w500,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Dra.',
          style: nameTextStyle,
        ),
        const SizedBox(height: 4),
        Text(appointment.doctor.name, style: valueTextStyle),
        const SizedBox(height: 12),
        Text(
          'Paciente',
          style: nameTextStyle,
        ),
        const SizedBox(height: 4),
        Text(appointment.patient.name, style: valueTextStyle),
      ],
    );
  }

  Widget _summarySection(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final fieldNameTextStyle = textTheme.bodyText2;
    final fieldValueTextStyle = textTheme.bodyText2?.copyWith(
      fontWeight: FontWeight.w500,
    );

    final title = _sectionTitle(context, 'Sumário');

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Status', style: fieldNameTextStyle),
            Text('Encerrada', style: fieldValueTextStyle),
          ],
        ),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Duração', style: fieldNameTextStyle),
            Text(appointment.duration.HHmmss, style: fieldValueTextStyle),
          ],
        ),
      ],
    );
  }

  Widget _ratingSection(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final fieldNameTextStyle = textTheme.bodyText2;
    final title = _sectionTitle(context, 'Deixe sua avaliação');

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title,
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Avaliação', style: fieldNameTextStyle),
            const SizedBox(width: 16),
            RatingBar(
              initialRating: 2,
              itemPadding: const EdgeInsets.symmetric(horizontal: 0),
              ratingWidget: RatingWidget(
                empty: const Icon(
                  Icons.star_outline_rounded,
                  color: Palette.primary,
                  size: 14,
                ),
                half: const Icon(
                  Icons.star_half_rounded,
                  color: Palette.primary,
                  size: 14,
                ),
                full: const Icon(
                  Icons.star_rounded,
                  color: Palette.primary,
                  size: 14,
                ),
              ),
              onRatingUpdate: (value) {},
            ),
          ],
        ),
        const SizedBox(height: 16),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            hintText: 'Comente sobre a sua experiência',
            labelText: 'Comentário',
          ),
          keyboardType: TextInputType.multiline,
          maxLines: 4,
          maxLength: 240,
          maxLengthEnforcement: MaxLengthEnforcement.enforced,
        ),
      ],
    );
  }

  Widget _sectionTitle(BuildContext context, String title) {
    final textStyle = Theme.of(context).textTheme.subtitle1?.copyWith(
          fontWeight: FontWeight.bold,
        );

    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Text(title, style: textStyle),
    );
  }
}
