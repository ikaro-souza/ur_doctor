import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/models/models.dart';
import 'package:ur_doctor/widgets/widgets.dart';
import 'widgets/widgets.dart';

class AppointmentCallRating extends StatelessWidget {
  static const String route = 'appointments/call/rating';

  const AppointmentCallRating({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    final title = _title(context);

    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
          children: [
            title,
            const SizedBox(height: 40),
            CallRatingCard(appointment: appointment),
            const SizedBox(height: 24),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MyElevatedButton(
                  height: 64,
                  label: 'Enviar avaliação',
                  onPressed: () => _onSubmitRatingTap(context),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final titleTextStyle = textTheme.headline1;
    final subtitleTextStyle = textTheme.bodyText2;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Sua chamada foi encerrada.',
          style: titleTextStyle,
        ),
        const SizedBox(height: 8),
        Text(
          'Por favor, avalie como foi a sua experiência.',
          style: subtitleTextStyle,
        ),
      ],
    );
  }

  void _onSubmitRatingTap(BuildContext context) {
    // TODO: Send appointment data to server

    Navigator.of(context).popUntil(
      (route) {
        return route.isFirst;
      },
    );
  }
}
