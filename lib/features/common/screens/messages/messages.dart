import 'package:flutter/material.dart';
import 'package:ur_doctor/widgets/widgets.dart';

import 'widgets/message_list.dart';

class Messages extends StatelessWidget {
  static const String route = 'messages';

  const Messages({
    Key? key,
    required this.onBackButtonTap,
  }) : super(key: key);

  final VoidCallback onBackButtonTap;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: FlatAppBar(
        title: 'Mensagens',
        onBackButtonTap: onBackButtonTap,
      ),
      body: const MessageList(),
    );
  }
}
