import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/state.dart';

import 'message_card.dart';

class MessageList extends StatelessWidget {
  const MessageList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final groups = groupBy(dummyMessages, (Message x) => x.appointment.id);
    List<Message> _messages = [];

    groups.forEach((key, messages) {
      _messages.add(messages[messages.length - 1]);
    });

    return ListView.separated(
      itemCount: _messages.length,
      padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
      itemBuilder: (c, i) {
        final message = _messages[i];

        return MessageCard(
          message: message,
          onTap: () => Navigator.of(context).pushNamed(
            Chat.route,
            arguments: message.appointment,
          ),
        );
      },
      separatorBuilder: (c, i) {
        return const SizedBox(height: 24);
      },
    );
  }
}
