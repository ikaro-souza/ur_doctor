import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/models/models.dart';

import 'message_card.dart';

class MessageList extends HookWidget {
  const MessageList({
    Key? key,
    required this.messages,
    required this.scrollController,
  }) : super(key: key);

  final List<Message> messages;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    final registeredPostframe = useState(false);
    if (!registeredPostframe.value) {
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        scrollController.animateTo(
          scrollController.position.maxScrollExtent,
          duration: kAnimationDuration,
          curve: kAnimationCurve,
        );
      });
    }

    return ListView.separated(
      controller: scrollController,
      itemCount: messages.length,
      padding: const EdgeInsets.fromLTRB(16, 24, 16, 88),
      itemBuilder: (_, i) {
        final message = messages[i];

        return MessageCard(
          message: message,
          style: message.sender == dummyUser
              ? MessageStyles.sent
              : MessageStyles.received,
        );
      },
      separatorBuilder: (_, i) {
        final message = messages[i];
        final next = i < messages.length - 1 ? messages[i + 1] : null;
        final isSameSender = message.sender.id == next?.sender.id;

        return SizedBox(height: isSameSender ? 8 : 16);
      },
    );
  }
}
