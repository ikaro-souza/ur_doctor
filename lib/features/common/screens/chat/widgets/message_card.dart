import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/state/models/models.dart';

class MessageCard extends StatelessWidget {
  const MessageCard({
    Key? key,
    required this.message,
    required this.style,
  }) : super(key: key);

  final Message message;
  final MessageStyles style;

  @override
  Widget build(BuildContext context) {
    final _style = style == MessageStyles.sent
        ? _MessageStyle.sent()
        : _MessageStyle.received();

    return Align(
      alignment: _style.alignment,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width * 0.75,
        ),
        child: Material(
          shape: _style.shape,
          color: _style.backgroundColor,
          child: InkWell(
            customBorder: _style.shape,
            child: _content(context, _style),
          ),
        ),
      ),
    );
  }

  Widget _content(BuildContext context, _MessageStyle style) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: _messageContent(textTheme, style),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            _timeStamp(textTheme, style),
          ],
        ),
      ],
    );
  }

  Widget _messageContent(TextTheme textTheme, _MessageStyle messageStyle) {
    final textStyle = textTheme.bodyText2!.copyWith(
      color: messageStyle.foregroundColor,
    );

    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 8, 32, 8),
      child: Text(message.content, style: textStyle),
    );
  }

  Widget _timeStamp(TextTheme textTheme, _MessageStyle messageStyle) {
    final textStyle = textTheme.caption!.copyWith(
      color: messageStyle.foregroundColor.withOpacity(.75),
    );

    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
      child: Text(DateFormat.Hm().format(message.sentTime), style: textStyle),
    );
  }
}

enum MessageStyles {
  sent,
  received,
}

class _MessageStyle {
  final AlignmentGeometry alignment;
  final Color backgroundColor;
  final Color foregroundColor;
  final ShapeBorder shape;

  _MessageStyle({
    required this.alignment,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.shape,
  });

  factory _MessageStyle.sent() {
    const radius = Radius.circular(12);

    return _MessageStyle(
      alignment: Alignment.topRight,
      backgroundColor: Palette.primary,
      foregroundColor: Palette.onPrimary,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: radius,
          bottomRight: radius,
          bottomLeft: radius,
        ),
      ),
    );
  }

  factory _MessageStyle.received() {
    const radius = Radius.circular(12);

    return _MessageStyle(
      alignment: Alignment.topLeft,
      backgroundColor: Palette.surface1,
      foregroundColor: Palette.onSurfaceVariant,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: radius,
          bottomRight: radius,
          bottomLeft: radius,
        ),
      ),
    );
  }
}
