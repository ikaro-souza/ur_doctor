import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class BottomControls extends StatefulWidget {
  const BottomControls({
    Key? key,
    required this.onSendMessageTap,
  }) : super(key: key);

  final void Function(String) onSendMessageTap;

  @override
  State<BottomControls> createState() => _BottomControlsState();
}

class _BottomControlsState extends State<BottomControls> {
  final _controller = TextEditingController();

  @override
  void initState() {
    _controller.addListener(() {
      setState(() {});
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(
          children: [
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: TextField(
                  controller: _controller,
                  decoration: const InputDecoration(
                    fillColor: Palette.surfaceVariant,
                    filled: true,
                    border: InputBorder.none,
                    hintText: 'Digite uma mensagem',
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.newline,
                ),
              ),
            ),
            const SizedBox(width: 12),
            MyIconButton(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              color: Palette.primaryContainer,
              icon: _controller.text.trim().isNotEmpty ? Icons.send : Icons.mic,
              iconColor: Palette.primary,
              size: 48,
              onTap: _controller.text.trim().isNotEmpty
                  ? () => _onSendMessageTap()
                  : () {},
            ),
          ],
        ),
      ),
    );
  }

  void _onSendMessageTap() {
    widget.onSendMessageTap(_controller.text);
    _controller.text = '';
  }
}
