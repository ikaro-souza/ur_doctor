import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/models/models.dart';
import 'package:ur_doctor/widgets/widgets.dart';
import './widgets/widgets.dart';

class Chat extends HookWidget {
  static const String route = '/chat';

  const Chat({Key? key, required this.appointment}) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    final _scrollController = useScrollController();
    final _messages = useState(List<Message>.from(dummyMessages)
        .where((x) => x.appointment.id == (appointment).id)
        .toList());
    final _appointment = appointment;

    return Scaffold(
      appBar: MyAppBar(
        title: _appointment.title,
        showBackButton: true,
      ),
      body: MessageList(
        messages: _messages.value,
        scrollController: _scrollController,
      ),
      bottomSheet: BottomSheet(
        backgroundColor: Palette.background,
        elevation: 0,
        enableDrag: false,
        builder: (BuildContext context) {
          return BottomControls(
            onSendMessageTap: (message) => _onSendMessageTap(
              _messages,
              _scrollController,
              message,
            ),
          );
        },
        onClosing: () {},
      ),
    );
  }

  void _onSendMessageTap(
    ValueNotifier<List<Message>> messageList,
    ScrollController scrollController,
    String newMessage,
  ) {
    final newList = List<Message>.from(messageList.value
      ..add(
        Message(
          id: messageList.value.length + 1,
          sender: dummyUser,
          appointment: appointment,
          content: newMessage,
          sentTime: DateTime.now(),
        ),
      ));
    messageList.value = newList;
  }
}
