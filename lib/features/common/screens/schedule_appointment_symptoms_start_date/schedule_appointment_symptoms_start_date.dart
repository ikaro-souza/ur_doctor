import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

import '../screens.dart';

class ScheduleAppointmentSymptomsStartDate extends StatefulWidget {
  static const String route = 'appointments/new/symptoms-start-date';

  const ScheduleAppointmentSymptomsStartDate({Key? key}) : super(key: key);

  @override
  _ScheduleAppointmentSymptomsStartDateState createState() =>
      _ScheduleAppointmentSymptomsStartDateState();
}

class _ScheduleAppointmentSymptomsStartDateState
    extends State<ScheduleAppointmentSymptomsStartDate> {
  final _textController = TextEditingController();

  DateTime? _date;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Agendar consulta',
        showBackButton: true,
      ),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(24, 48, 24, 0),
            sliver: SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                children: [
                  _buildTitleAndDescription(context),
                  const SizedBox(height: 56),
                  _buildForm(),
                  const Spacer(),
                  const SizedBox(height: 16),
                  MyElevatedButton(
                    borderRadius: 16,
                    disabled: _date == null,
                    height: 64,
                    icon: const Icon(Icons.arrow_forward),
                    label: 'Próximo',
                    onPressed: () => Navigator.of(context).pushNamed(
                      ScheduleAppointmentDescription.route,
                    ),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Quando eles começaram?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Informe quando começou a sentir seus sintomas.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(16),
            child: SizedBox(
              height: 64,
              child: TextField(
                autofocus: true,
                controller: _textController,
                decoration: const InputDecoration(
                  fillColor: Palette.surfaceVariant,
                  filled: true,
                  border: InputBorder.none,
                  labelText: 'Data de início dos sintomas',
                  suffixIcon: Icon(Icons.calendar_today),
                ),
                readOnly: true,
                onTap: () {
                  showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    firstDate: DateTime.now().subtract(
                      const Duration(days: 365 * 100),
                    ),
                    lastDate: DateTime.now(),
                    helpText: 'Selecione a data de início dos sintomas',
                  ).then((value) {
                    if (value == null) return;

                    setState(() {
                      _date = value;
                      _textController.text =
                          DateFormat('dd/MM/yyyy').format(_date!);
                    });
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
