import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

import '../screens.dart';

class ScheduleAppointmentDescription extends StatefulWidget {
  static const String route = 'appointments/new/description';

  const ScheduleAppointmentDescription({Key? key}) : super(key: key);

  @override
  _ScheduleAppointmentDescriptionState createState() =>
      _ScheduleAppointmentDescriptionState();
}

class _ScheduleAppointmentDescriptionState
    extends State<ScheduleAppointmentDescription> {
  final _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Agendar consulta',
        showBackButton: true,
      ),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(24, 48, 24, 0),
            sliver: SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                children: [
                  _buildTitleAndDescription(context),
                  const SizedBox(height: 56),
                  _buildForm(),
                  const Spacer(),
                  const SizedBox(height: 16),
                  MyElevatedButton(
                    borderRadius: 16,
                    disabled: _textController.text.trim().length < 10,
                    height: 64,
                    icon: const Icon(Icons.arrow_forward),
                    label: 'Próximo',
                    onPressed: () => Navigator.of(context).pushNamed(
                      ScheduleAppointmentConfirm.route,
                    ),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Como você está se sentindo?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Descreva o que está sentindo.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm() {
    return Row(
      children: [
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(16),
            child: ConstrainedBox(
              constraints: const BoxConstraints(minHeight: 64),
              child: TextField(
                autofocus: true,
                controller: _textController,
                decoration: const InputDecoration(
                  fillColor: Palette.surfaceVariant,
                  filled: true,
                  border: InputBorder.none,
                  hintText: 'Estou sentindo falta de ar constante..',
                ),
                keyboardType: TextInputType.multiline,
                minLines: 2,
                maxLines: 10,
                textInputAction: TextInputAction.newline,
                onChanged: (_) {
                  setState(() {});
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
