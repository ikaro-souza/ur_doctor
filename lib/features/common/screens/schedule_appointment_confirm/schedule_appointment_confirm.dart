import 'package:flutter/material.dart';
import 'package:ur_doctor/features/common/utils/utils.dart';
import 'package:ur_doctor/widgets/widgets.dart';
import '../../state/mock.dart';
import '../../state/state.dart';

class ScheduleAppointmentConfirm extends StatelessWidget {
  static const String route = 'appointments/new/confirm';

  const ScheduleAppointmentConfirm({Key? key}) : super(key: key);

  Appointment get _appointment => Appointment(
        id: dummyAppointments.length + 1,
        title: 'Falta de ar, febre e tosse',
        date: DateTime.now().add(const Duration(days: 7)),
        duration: const Duration(minutes: 30),
        doctor: dummyDoctor,
        patient: dummyPatient,
        symptoms: ['Falta de ar', 'Febre', 'Tosse'],
        symptomsStartDate: DateTime.now().subtract(const Duration(days: 7)),
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mattis dolor feugiat non tempor.',
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Agendar consulta',
        showBackButton: true,
      ),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(24, 48, 24, 0),
            sliver: SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                children: [
                  _titleAndDescription(context),
                  const SizedBox(height: 24),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _informationTile(
                        context,
                        'Data',
                        formatAppointmentSchedulingDate(_appointment.date),
                      ),
                      const SizedBox(height: 12),
                      _informationTile(
                        context,
                        'Com sintomas de',
                        formatSymptoms(_appointment.symptoms),
                      ),
                      const SizedBox(height: 12),
                      _informationTile(
                        context,
                        'Desde',
                        formatSymptomsStartDate(_appointment.symptomsStartDate),
                      ),
                      const SizedBox(height: 12),
                      _informationTile(
                        context,
                        'Descrição',
                        _appointment.description,
                      ),
                    ],
                  ),
                  const SizedBox(height: 16),
                  const Spacer(),
                  MyElevatedButton(
                    borderRadius: 16,
                    height: 64,
                    label: 'Agendar',
                    onPressed: () => _scheduleAppointment(context),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _titleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Confirme as informações.', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Confira se as informações sobre do agendamento estão corretas.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _informationTile(BuildContext context, String name, String value) {
    final textTheme = Theme.of(context).textTheme;
    final nameTextStyle = textTheme.caption;
    final valueTextStyle = textTheme.bodyText2!.copyWith(
      fontWeight: FontWeight.w500,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name,
          style: nameTextStyle,
        ),
        const SizedBox(height: 4),
        Text(value, style: valueTextStyle),
      ],
    );
  }

  void _scheduleAppointment(BuildContext context) {
    dummyAppointments.add(_appointment);

    Navigator.of(context).popUntil(
      (route) => route.isFirst,
    );
  }
}
