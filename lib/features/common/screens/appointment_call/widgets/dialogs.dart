import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';

Widget buildConfirmEndCallCupertinoDialog(BuildContext context) =>
    CupertinoAlertDialog(
      title: const Text('Encerrar chamada'),
      content: const Text(
        'Tem certeza que deseja encerrar a chamada? Essa ação é irreversível e também irá encerrar a consulta.',
      ),
      actions: [
        CupertinoDialogAction(
          isDestructiveAction: true,
          onPressed: () => Navigator.of(context).pop(true),
          child: const Text('Sim'),
        ),
        CupertinoDialogAction(
          isDefaultAction: true,
          onPressed: () => Navigator.of(context).pop(false),
          child: const Text('Não'),
        )
      ],
    );

Widget buildConfirmEndCallDialog(BuildContext context) {
  final textStyle = Theme.of(context).textTheme.button!.copyWith(
        color: Palette.onSurface,
      );
  final destructiveTextStyle = textStyle.copyWith(color: Palette.error);

  return AlertDialog(
    title: const Text('Encerrar chamada'),
    content: const Text(
      'Tem certeza que deseja encerrar a chamada? Essa ação é irreversível e também irá encerrar a consulta.',
    ),
    actions: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: TextButton(
              child: Text('Sim', style: destructiveTextStyle),
              onPressed: () => Navigator.of(context).pop(true),
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('Não', style: textStyle),
            ),
          ),
        ],
      )
    ],
    buttonPadding: const EdgeInsets.symmetric(horizontal: 12),
  );
}
