import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/widgets/widgets.dart';
import './dialogs.dart';

class Toolbar extends StatelessWidget {
  const Toolbar({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MyIconButton(
          boxShadow: kHighElevation,
          color: Palette.surface,
          icon: Icons.chevron_left,
          iconColor: Palette.onSurface,
          onTap: () => Navigator.of(context).pop(),
        ),
        MyIconButton(
          boxShadow: kHighElevation,
          color: Palette.errorContainer,
          icon: Icons.phone_disabled,
          iconColor: Palette.onErrorContainer,
          onTap: () => _onEndCallTap(context),
        )
      ],
    );
  }

  void _onEndCallTap(BuildContext context) async {
    Future<bool?> future = Platform.isIOS
        ? showCupertinoDialog(
            context: context,
            builder: buildConfirmEndCallCupertinoDialog,
          )
        : showDialog(
            context: context,
            builder: buildConfirmEndCallDialog,
          );

    final result = await future;
    if (result == true) {
      // TODO: Update appointment data in state before navigating to next route
      Navigator.of(context).pushReplacementNamed(
        AppointmentCallRating.route,
        arguments: appointment,
      );
    }
  }
}
