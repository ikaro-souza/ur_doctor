import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class CameraFeedback extends StatelessWidget {
  const CameraFeedback({
    Key? key,
    this.heightFactor = .4,
    this.borderRadius = 12,
  }) : super(key: key);

  final double heightFactor;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return FittedBox(
      child: AnimatedContainer(
        curve: kAnimationCurve,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius),
          boxShadow: kHighElevation,
        ),
        duration: kAnimationDuration,
        height: screenSize.height * .25,
        child: AspectRatio(
          aspectRatio: 3 / 4,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(borderRadius),
            child: Image.asset(
              'assets/images/patient.png',
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
    );
  }
}
