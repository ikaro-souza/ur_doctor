import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/common/state/state.dart';
import 'package:ur_doctor/features/common/utils/utils.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class AppointmentCallBottomSheet extends HookWidget {
  const AppointmentCallBottomSheet({
    Key? key,
    required this.ellapsedSeconds,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;
  final actionSpacing = 24.0;
  final int ellapsedSeconds;

  @override
  Widget build(BuildContext context) {
    final micIsMuted = useState(false);

    return Container(
      decoration: BoxDecoration(
        color: Palette.surface1,
        boxShadow: kUpwardsHighElevation,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(32),
        ),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 32,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              MyIconButton(
                color: Palette.secondaryContainer,
                icon: !micIsMuted.value ? Icons.mic_off : Icons.mic,
                iconColor: Palette.onSecondaryContainer,
                onTap: () => _onMicTap(micIsMuted),
              ),
              SizedBox(width: actionSpacing),
              MyIconButton(
                color: Palette.secondaryContainer,
                icon: Icons.cameraswitch,
                iconColor: Palette.onSecondaryContainer,
                onTap: () {},
              ),
              SizedBox(width: actionSpacing),
              MyIconButton(
                color: Palette.primary,
                icon: Icons.message,
                iconColor: Palette.onPrimary,
                onTap: () => Navigator.of(context).pushNamed(
                  Chat.route,
                  arguments: appointment,
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          _timer(context),
        ],
      ),
    );
  }

  Widget _timer(BuildContext context) {
    final textStyle = Theme.of(context).textTheme.bodyText1!.copyWith(
          color: Palette.onSecondaryContainer,
        );

    return Text(
      Duration(seconds: ellapsedSeconds).HHmmss,
      textAlign: TextAlign.center,
      style: textStyle,
    );
  }

  void _onMicTap(ValueNotifier<bool> micIsMuted) {
    micIsMuted.value = !micIsMuted.value;
  }
}
