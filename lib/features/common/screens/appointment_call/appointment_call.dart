import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:ur_doctor/features/common/state/state.dart';

import './widgets/widgets.dart';

class AppointmentCall extends HookWidget {
  static const String route = 'appointments/call';

  const AppointmentCall({
    Key? key,
    required this.appointment,
  }) : super(key: key);

  final Appointment appointment;

  @override
  Widget build(BuildContext context) {
    final ellapsedTime = useState(0);
    _startTimer(ellapsedTime);

    return Scaffold(
      body: SafeArea(
        top: true,
        child: Stack(
          clipBehavior: Clip.hardEdge,
          children: [
            Positioned.fill(
              child: Image.asset(
                'assets/images/doctor.png',
                fit: BoxFit.fill,
              ),
            ),
            Positioned.fill(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(32, 40, 32, 0),
                    child: Toolbar(appointment: appointment),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(right: 32),
                        child: CameraFeedback(),
                      ),
                      const SizedBox(height: 40),
                      AppointmentCallBottomSheet(
                        ellapsedSeconds: ellapsedTime.value,
                        appointment: appointment,
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _startTimer(ValueNotifier<int> ellapsedTime) {
    if (ellapsedTime.value > 0) return;

    Timer.periodic(const Duration(seconds: 1), (timer) {
      try {
        ellapsedTime.value++;
      } catch (e) {
        timer.cancel();
      }
    });
  }
}
