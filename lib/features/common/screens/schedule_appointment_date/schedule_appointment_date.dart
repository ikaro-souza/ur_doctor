import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/features/common/state/mock.dart';
import 'package:ur_doctor/features/common/state/models/appointment_schedule_form.dart';
import 'package:ur_doctor/widgets/widgets.dart';

import '../schedule_appointment_symptoms/schedule_appointment_symptoms.dart';

class ScheduleAppointmentDate extends StatefulWidget {
  static const String route = 'appointments/new/date';

  const ScheduleAppointmentDate({Key? key}) : super(key: key);

  @override
  State<ScheduleAppointmentDate> createState() =>
      _ScheduleAppointmentDateState();
}

class _ScheduleAppointmentDateState extends State<ScheduleAppointmentDate> {
  final _dateTextController = TextEditingController();
  final _timeTextController = TextEditingController();

  DateTime? _date;
  bool _enableNextButton = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Agendar consulta',
        showBackButton: true,
      ),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(24, 48, 24, 0),
            sliver: SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildTitleAndDescription(context),
                  const SizedBox(height: 56),
                  _buildForm(),
                  const Spacer(),
                  const SizedBox(height: 16),
                  MyElevatedButton(
                    borderRadius: 16,
                    disabled: !_enableNextButton,
                    height: 64,
                    label: 'Próximo',
                    icon: const Icon(Icons.arrow_forward),
                    onPressed: () => Navigator.of(context).pushNamed(
                      ScheduleAppointmentSymptoms.route,
                      arguments: AppointmentScheduleForm(
                        patient: dummyUser,
                        date: _date,
                      ),
                    ),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Para quando quer sua consulta?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Consulte abaixo a disponibilidade do médico e escolha sua data e hora de preferência.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm() {
    return Row(
      children: [
        Expanded(
          flex: 6,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(16),
            child: TextField(
              autofocus: true,
              readOnly: true,
              controller: _dateTextController,
              decoration: const InputDecoration(
                fillColor: Palette.surfaceVariant,
                filled: true,
                border: InputBorder.none,
                labelText: 'Data da consulta',
                suffixIcon: Icon(Icons.calendar_today),
              ),
              onTap: () {
                showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                  firstDate: DateTime.now(),
                  lastDate: DateTime(DateTime.now().year + 1, 0, 0),
                  helpText: 'Selecione a data da consulta',
                ).then((value) {
                  if (value == null) return;

                  setState(() {
                    _date = value;
                    _dateTextController.text =
                        DateFormat('dd/MM/yyyy').format(_date!);
                  });
                });
              },
            ),
          ),
        ),
        const SizedBox(width: 16),
        Expanded(
          flex: 4,
          child: ClipRRect(
            // TODO: Turn into dropdown field
            borderRadius: BorderRadius.circular(16),
            child: TextField(
              controller: _timeTextController,
              decoration: const InputDecoration(
                fillColor: Palette.surfaceVariant,
                filled: true,
                border: InputBorder.none,
                labelText: 'Hora',
                suffixIcon: Icon(Icons.access_time_filled),
              ),
              readOnly: true,
              enabled: _date != null,
              onTap: () {
                showTimePicker(
                  context: context,
                  // TODO: Parameterize doctor's initial scheduling time and appointment duration
                  initialTime: const TimeOfDay(hour: 9, minute: 0),
                  initialEntryMode: TimePickerEntryMode.dial,
                  helpText: 'Selecione a hora da consulta',
                ).then((value) {
                  if (value == null) return;

                  final updatedDate = DateTime(
                    _date!.year,
                    _date!.month,
                    _date!.day,
                    value.hour,
                    value.minute,
                  );

                  setState(() {
                    _date = updatedDate;
                    _timeTextController.text = DateFormat.Hm().format(_date!);
                    _enableNextButton = true;
                  });
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}
