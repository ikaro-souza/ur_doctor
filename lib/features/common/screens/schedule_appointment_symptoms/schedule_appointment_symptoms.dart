import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';
import 'package:ur_doctor/widgets/widgets.dart';

import '../schedule_appointment_symptoms_start_date/schedule_appointment_symptoms_start_date.dart';

class ScheduleAppointmentSymptoms extends StatefulWidget {
  static const String route = 'appointments/new/symptoms';

  const ScheduleAppointmentSymptoms({Key? key}) : super(key: key);

  @override
  SschedulPappointmentSymptomsState createState() =>
      SschedulPappointmentSymptomsState();
}

class SschedulPappointmentSymptomsState
    extends State<ScheduleAppointmentSymptoms> {
  final _textController = TextEditingController();

  final List<String> _symptoms = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: 'Agendar consulta',
        showBackButton: true,
      ),
      body: CustomScrollView(
        slivers: [
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(24, 48, 24, 0),
            sliver: SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                children: [
                  _buildTitleAndDescription(context),
                  const SizedBox(height: 56),
                  _buildForm(),
                  const Spacer(),
                  const SizedBox(height: 16),
                  MyElevatedButton(
                    borderRadius: 16,
                    disabled: _symptoms.isEmpty,
                    height: 64,
                    icon: const Icon(Icons.arrow_forward),
                    label: 'Próximo',
                    onPressed: () => Navigator.of(context).pushNamed(
                      ScheduleAppointmentSymptomsStartDate.route,
                    ),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitleAndDescription(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Quais os seus sintomas?', style: textTheme.headline1),
        const SizedBox(height: 8),
        Text(
          'Liste os sintomas que está sentindo.',
          style: textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _buildForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: SizedBox(
                  height: 64,
                  child: TextField(
                    autofocus: true,
                    controller: _textController,
                    decoration: const InputDecoration(
                      fillColor: Palette.surfaceVariant,
                      filled: true,
                      border: InputBorder.none,
                      labelText: 'Sintoma',
                    ),
                    onChanged: (_) {
                      setState(() {});
                    },
                  ),
                ),
              ),
            ),
            const SizedBox(width: 16),
            MyIconButton(
              color: Palette.primaryContainer,
              disabled: _textController.text.isEmpty,
              icon: Icons.add_circle,
              iconColor: Palette.onPrimaryContainer,
              size: 64,
              onTap: () {
                setState(() {
                  _symptoms.add(_textController.text);
                  _textController.text = '';
                });
              },
            )
          ],
        ),
        const SizedBox(height: 8),
        Wrap(
          children: _symptoms.map((e) {
            final chip = Chip(label: Text(e));
            return _symptoms.indexOf(e) == _symptoms.length - 1
                ? chip
                : Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      chip,
                      const SizedBox(width: 8),
                    ],
                  );
          }).toList(),
        )
      ],
    );
  }
}
