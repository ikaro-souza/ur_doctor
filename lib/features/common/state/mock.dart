import 'dart:math';

import 'models/models.dart';

final dummyPatient = User.patient(
  id: 1,
  name: 'Ana Vitória Alves da Silva',
  taxId: '94082064010',
  phone: '85998791134',
  email: 'vick_izinha@gmail.com',
  displayName: 'Ana',
);

final dummyDoctor = User.doctor(
  id: 2,
  name: 'Anon Chan',
  taxId: '27348899005',
  license: '71524889-9/CE',
  phone: '85983214681',
  email: 'dr.chan@gmail.com',
);

final dummyUser = dummyPatient;

final dummyAppointments = [
  Appointment(
    id: 1,
    title: 'Avaliação de exame de sangue',
    date: DateTime.now().add(const Duration(days: 7)),
    duration: const Duration(minutes: 30),
    doctor: dummyDoctor,
    patient: dummyPatient,
    symptoms: [],
    symptomsStartDate: DateTime.now().subtract(const Duration(days: 3)),
    description: '',
  ),
  Appointment(
    id: 2,
    title: 'Sintomas de covid',
    date: DateTime.now().add(const Duration(days: 15)),
    duration: const Duration(minutes: 60),
    doctor: dummyDoctor,
    patient: dummyPatient,
    symptoms: [],
    symptomsStartDate: DateTime.now().subtract(const Duration(days: 3)),
    description: '',
  ),
  Appointment(
    id: 3,
    title: 'Retorno: Sintomas de covid',
    date: DateTime.now().add(const Duration(days: 30)),
    duration: const Duration(minutes: 30),
    doctor: dummyDoctor,
    patient: dummyPatient,
    symptoms: [],
    symptomsStartDate: DateTime.now().subtract(const Duration(days: 3)),
    description: '',
  )
];

final dummyMessages = List.generate(
  30,
  (index) {
    final random = Random();
    final appointment =
        dummyAppointments[random.nextInt(dummyAppointments.length)];
    final content =
        _dummyMessageContent[random.nextInt(_dummyMessageContent.length - 1)];
    final sender = random.nextBool() ? dummyPatient : dummyDoctor;

    return Message(
      id: index,
      content: content,
      appointment: appointment,
      sender: sender,
      sentTime: DateTime.now(),
    );
  },
);

final _dummyMessageContent = [
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus laoreet consectetur.',
  'Ursus curabitur sollicitudin erat blandit habitant est in risus, primis gravida tellus torquent ipsum diam. Habitasse elementum tristique eros metus consectetur porta aliquam morbi, commodo eros lorem volutpat aptent blandit convallis.',
  'Nisl habitasse nam magna fringilla odio ut et, aliquam varius ornare porta dictum id fermentum, gravida duis neque.',
  'Mollis nunc sed id semper risus in hendrerit gravida rutrum. Imperdiet dui accumsan sit amet nulla facilisi.',
  'Sit amet massa vitae tortor condimentum lacinia quis vel eros',
  'Est ullamcorper eget nulla facilisi etiam dignissim diam. Maecenas ultricies mi eget mauris pharetra et ultrices. Rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Donec ultrices tincidunt arcu non sodales neque sodales ut. In hac habitasse platea dictumst.',
  'Volutpat commodo sed egestas egestas fringilla phasellus',
  'Tortor id aliquet lectus proin nibh nisl condimentum id. Facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum.',
  'Lectus proin nibh nisl condimentum id venenatis a condimentum. Turpis egestas integer eget aliquet nibh praesent. Adipiscing commodo elit at imperdiet.',
  'Tellus pellentesque eu tincidunt tortor aliquam nulla'
];
