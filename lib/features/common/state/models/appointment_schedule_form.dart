import 'package:ur_doctor/features/authentication/state/models/models.dart';

class AppointmentScheduleForm {
  final User? patient;
  final DateTime? date;
  final List<String>? symptoms;
  final DateTime? symptomsStartDate;
  final String? description;

  AppointmentScheduleForm({
    this.patient,
    this.date,
    this.symptoms,
    this.symptomsStartDate,
    this.description,
  });

  AppointmentScheduleForm copyWith({
    User? patient,
    DateTime? date,
    List<String>? symptoms,
    DateTime? symptomsStartDate,
    String? description,
  }) {
    return AppointmentScheduleForm(
      patient: patient ?? this.patient,
      date: date ?? this.date,
      symptoms: symptoms ?? this.symptoms,
      symptomsStartDate: symptomsStartDate ?? this.symptomsStartDate,
      description: description ?? this.description,
    );
  }
}
