import 'appointment.dart';
import '../../../authentication/state/models/user.dart';

class Message {
  final int id;
  final String content;
  final Appointment appointment;
  final User sender;
  final DateTime sentTime;

  const Message({
    required this.id,
    required this.content,
    required this.appointment,
    required this.sender,
    required this.sentTime,
  });
}
