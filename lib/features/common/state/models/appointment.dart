import 'models.dart';

class Appointment {
  final int id;
  final String title;
  final DateTime date;
  final Duration duration;
  final User doctor;
  final User patient;
  final List<String> symptoms;
  final DateTime symptomsStartDate;
  final String description;

  const Appointment({
    required this.id,
    required this.title,
    required this.date,
    required this.duration,
    required this.doctor,
    required this.patient,
    required this.symptoms,
    required this.symptomsStartDate,
    required this.description,
  });
}
