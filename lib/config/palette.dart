import 'package:flutter/material.dart';

class Palette {
  static const Color primary = Color(0xFF006C51);
  static const Color onPrimary = Color(0xFFFFFFFF);
  static const Color primaryContainer = Color(0xFF45FEC9);
  static const Color onPrimaryContainer = Color(0xFF002116);

  static const Color secondary = Color(0xFF4C6359);
  static const Color onSecondary = Color(0xFFFFFFFF);
  static const Color secondaryContainer = Color(0xFFCEE8DB);
  static const Color onSecondaryContainer = Color(0xFF082018);

  static const Color terciary = Color(0xFF3F6375);
  static const Color onTerciary = Color(0xFFFFFFFF);
  static const Color terciaryContainer = Color(0xFFC2E8FD);
  static const Color onTerciaryContainer = Color(0xFF001E2B);

  static const Color error = Color(0xFFBA1B1B);
  static const Color onRrror = Color(0xFFFFFFFF);
  static const Color errorContainer = Color(0xFFFFDAD4);
  static const Color onErrorContainer = Color(0xFF410001);

  static const Color background = Color(0xFFFAFDF9);
  static const Color onBackground = Color(0xFF191C1B);

  static const Color surface = Color(0xFFFAFDF9);
  static const Color onSurface = Color(0xFF191C1B);
  static const Color surfaceVariant = Color(0xFFDBE5DE);
  static const Color onSurfaceVariant = Color(0xFF3F4944);
  static const Color disabledSurface = Color(0x1F1F1F1F);

  static final Color surface1 = Color.alphaBlend(
    primary.withAlpha(Color.getAlphaFromOpacity(.05)),
    background,
  );
  static final Color surface2 = Color.alphaBlend(
    primary.withAlpha(Color.getAlphaFromOpacity(.08)),
    background,
  );

  static const Color outline = Color(0xFF707974);
}
