import 'package:flutter/animation.dart';

const kAnimationDuration = Duration(milliseconds: 120);
const kAnimationCurve = Curves.easeInQuint;
const kPageTransitionDuration = Duration(milliseconds: 120);
const kPageTransitionCurve = Curves.easeInOutCubic;
