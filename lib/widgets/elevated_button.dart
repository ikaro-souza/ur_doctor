import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';

class MyElevatedButton extends StatelessWidget {
  const MyElevatedButton({
    Key? key,
    this.height = 56,
    this.borderRadius = 12,
    this.icon,
    this.onPressed,
    this.padding = const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
    this.disabled = false,
    required this.label,
  }) : super(key: key);

  final double height;
  final double borderRadius;
  final String label;
  final Icon? icon;
  final EdgeInsets? padding;
  final VoidCallback? onPressed;
  final bool disabled;
  final double _iconSize = 24;

  BorderRadius get _borderRadius => BorderRadius.circular(borderRadius);

  double get _effectiveIconSize => icon != null ? _iconSize : 0;

  double get _spacing => icon != null ? 16 : 0;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: disabled ? Theme.of(context).disabledColor : Palette.primary,
      shape: RoundedRectangleBorder(borderRadius: _borderRadius),
      child: InkWell(
        borderRadius: _borderRadius,
        onTap: disabled ? null : onPressed,
        child: Container(
          padding: padding,
          height: height,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 12),
                blurRadius: 15,
                color: Colors.black.withOpacity(.1),
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: _spacing),
                  child: Text(
                    label,
                    style: _getLabelStyle(context),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: _effectiveIconSize,
                width: _effectiveIconSize,
                child: icon != null
                    ? IconTheme(
                        data: _getIconTheme(context),
                        child: icon!,
                      )
                    : null,
              )
            ],
          ),
        ),
      ),
    );
  }

  TextStyle _getLabelStyle(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .button!
        .copyWith(color: Palette.onPrimary);
  }

  IconThemeData _getIconTheme(BuildContext context) {
    return Theme.of(context).iconTheme.copyWith(color: Palette.onPrimary);
  }
}
