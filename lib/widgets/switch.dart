import 'package:flutter/material.dart';
import 'package:ur_doctor/config/animation.dart';
import 'package:ur_doctor/config/palette.dart';

class MySwitch extends StatefulWidget {
  const MySwitch({
    Key? key,
    this.checked = false,
    this.label,
    this.onChanged,
  }) : super(key: key);

  final bool checked;
  final String? label;
  final Function(bool)? onChanged;

  @override
  _MySwitchState createState() => _MySwitchState();
}

class _MySwitchState extends State<MySwitch> {
  @override
  Widget build(BuildContext context) {
    var switchWidget = _buildSwitch();
    switchWidget =
        widget.label == null ? switchWidget : _withLabel(switchWidget);

    return GestureDetector(
      onTap: () => widget.onChanged?.call(!widget.checked),
      child: switchWidget,
    );
  }

  Widget _buildSwitch() {
    return Stack(
      children: [
        AnimatedContainer(
          duration: kAnimationDuration,
          curve: kAnimationCurve,
          height: 20,
          width: 36,
          decoration: BoxDecoration(
            color: widget.checked ? Palette.primary : Palette.surfaceVariant,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        Positioned.fill(
          left: widget.checked ? 18 : 2,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 16,
              width: 16,
              decoration: BoxDecoration(
                color: Palette.surface,
                borderRadius: BorderRadius.circular(16),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _withLabel(Widget switchWidget) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        switchWidget,
        const SizedBox(width: 12),
        Text(
          widget.label!,
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ],
    );
  }
}
