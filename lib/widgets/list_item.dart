import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';

class ListItem extends StatelessWidget {
  const ListItem({
    Key? key,
    required this.label,
    this.leading,
    this.metadata,
    this.onTap,
    this.selected = false,
  }) : super(key: key);

  final String label;
  final bool selected;
  final Widget? leading;
  final Widget? metadata;
  final VoidCallback? onTap;

  final _height = 56.0;
  final _contentGap = 12.0;

  BorderRadius get _borderRadius => BorderRadius.circular(56);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        textTheme: const TextTheme(
          subtitle1: TextStyle(
            color: Palette.onSecondaryContainer,
            fontWeight: FontWeight.w700,
            height: 1.43,
            letterSpacing: .1,
          ),
        ),
      ),
      child: Builder(
        builder: (context) {
          return Material(
            borderRadius: _borderRadius,
            child: InkWell(
              onTap: onTap,
              borderRadius: _borderRadius,
              child: Ink(
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    borderRadius: _borderRadius,
                  ),
                  color:
                      selected ? Palette.secondaryContainer : Palette.surface1,
                ),
                child: SizedBox(
                  height: _height,
                  child: _buildContent(context),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    final _leading = _buildLeadingWidget(context);
    final _label = _buildLabel(context);
    final _trailing = _buildTrailing(context);

    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 24, 16),
      child: Row(
        children: [
          _leading,
          SizedBox(width: leading != null ? _contentGap : 0),
          Expanded(child: _label),
          SizedBox(width: metadata != null ? _contentGap : 0),
          _trailing,
        ],
      ),
    );
  }

  SizedBox _buildLeadingWidget(BuildContext context) {
    const leadingSize = 24.0;

    return SizedBox(
      height: leading != null ? leadingSize : 0,
      width: leading != null ? leadingSize : 0,
      child: leading,
    );
  }

  Widget _buildLabel(BuildContext context) {
    return Text(label, style: Theme.of(context).textTheme.subtitle1!);
  }

  Widget _buildTrailing(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: 32,
      ),
      child: AnimatedDefaultTextStyle(
        duration: kAnimationDuration,
        style: Theme.of(context).textTheme.subtitle1!,
        child: metadata ?? const SizedBox(width: 0),
      ),
    );
  }
}

// class _MyListItemState extends State<MyListItem> {
//   final _height = 56.0;
//   final _borderRadius = 100.0;

//   bool _selected = false;

//   Color get _backgroundColor =>
//       _selected ? Palette.secondaryContainer : Colors.transparent;

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     _selected = widget.selected;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Theme(
//       data: ThemeData(
//         iconTheme: const IconThemeData(
//           color: Palette.onSecondaryContainer,
//           size: 24,
//         ),
//       ),
//       child: SizedBox(
//         height: _height,
//         child: Material(
//           borderRadius: BorderRadius.circular(_borderRadius),
//           textStyle: const TextStyle(
//             color: Palette.onSecondaryContainer,
//             height: 1.43,
//             letterSpacing: 0.1,
//             fontSize: 14,
//             fontWeight: FontWeight.bold,
//           ),
//           color: Colors.transparent,
//           child: InkWell(
//             onTap: _onTap,
//             borderRadius: BorderRadius.circular(_borderRadius),
//             child: ClipRRect(
//               borderRadius: BorderRadius.circular(_borderRadius),
//               child: _buildContent(),
//             ),
//           ),
//         ),
//       ),
//     );
//   }


// }
