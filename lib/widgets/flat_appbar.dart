import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/widgets/widgets.dart';

class FlatAppBar extends StatelessWidget implements PreferredSizeWidget {
  const FlatAppBar({
    Key? key,
    required this.title,
    required this.onBackButtonTap,
  }) : super(key: key);

  final String title;
  final VoidCallback onBackButtonTap;

  @override
  Size get preferredSize => const Size.fromHeight(64);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: SizedBox(
        height: preferredSize.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: NavigationToolbar(
            centerMiddle: false,
            leading: _buildLeading(context),
            middle: _buildMiddle(context),
            middleSpacing: 24,
          ),
        ),
      ),
    );
  }

  Widget _buildLeading(BuildContext context) {
    final borderRadius = BorderRadius.circular(12);
    const size = 48.0;

    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: (preferredSize.height - size) / 2,
      ),
      child: SizedBox(
        width: size,
        child: Material(
          shape: RoundedRectangleBorder(borderRadius: borderRadius),
          type: MaterialType.button,
          color: Palette.surface,
          child: InkWell(
            borderRadius: borderRadius,
            onTap: onBackButtonTap,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: borderRadius,
                boxShadow: kHighElevation,
                color: Palette.surface,
              ),
              child: const Icon(
                Icons.chevron_left,
                size: 24,
                color: Palette.onSurface,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMiddle(BuildContext context) {
    final textStyle = Theme.of(context)
        .textTheme
        .headline4!
        .copyWith(fontWeight: FontWeight.bold);

    return Text(
      title,
      style: textStyle,
    );
  }
}
