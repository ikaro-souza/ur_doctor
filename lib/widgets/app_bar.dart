import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  const MyAppBar({
    Key? key,
    this.showBackButton,
    this.action,
    required this.title,
  }) : super(key: key);

  final String title;
  final bool? showBackButton;
  final Widget? action;

  @override
  Size get preferredSize => const Size.fromHeight(80);

  @override
  State<MyAppBar> createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  double get height => widget.preferredSize.height;

  BorderRadius get _outerBorderRadius => const BorderRadius.vertical(
        bottom: Radius.circular(36),
      );

  TextStyle get _titleTextStyle =>
      Theme.of(context).textTheme.bodyText1!.copyWith(
            color: Palette.onSurface,
            height: 1.5,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          );

  @override
  Widget build(BuildContext context) {
    final content = _buildContent();

    return SafeArea(
      top: true,
      child: _buildBackground(child: content),
    );
  }

  Widget _buildContent() {
    double actionSize = widget.action != null ? 24 : 0;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          child: widget.showBackButton == true ? _buildBackButton() : null,
        ),
        Expanded(
          child: Padding(
            padding: _getTitlePadding(),
            child: Text(
              widget.title,
              textAlign: TextAlign.center,
              style: _titleTextStyle,
            ),
          ),
        ),
        SizedBox(
          height: actionSize,
          width: actionSize,
          child: widget.action,
        ),
      ],
    );
  }

  EdgeInsets _getTitlePadding() {
    bool showBackButton =
        widget.showBackButton != null && widget.showBackButton == true;
    bool hasAction = widget.action != null;
    if (showBackButton && hasAction) {
      return EdgeInsets.zero;
    } else if (showBackButton) {
      return const EdgeInsets.fromLTRB(0, 0, 24, 0);
    } else if (hasAction) {
      return const EdgeInsets.fromLTRB(24, 0, 0, 0);
    }
    return EdgeInsets.zero;
  }

  Widget _buildBackButton() {
    return SizedBox(
      height: 24,
      width: 24,
      child: IconButton(
        icon: const Icon(Icons.chevron_left),
        padding: EdgeInsets.zero,
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Widget _buildBackground({Widget? child}) {
    return Container(
      height: height,
      decoration: BoxDecoration(
        color: Palette.surface,
        borderRadius: _outerBorderRadius,
        boxShadow: const [
          BoxShadow(
            blurRadius: 40,
            offset: Offset(0, 16),
            color: Colors.black12,
          )
        ],
      ),
      child: ClipRRect(
        borderRadius: _outerBorderRadius,
        child: Material(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 28, 20, 20),
            child: child,
          ),
        ),
      ),
    );
  }
}
