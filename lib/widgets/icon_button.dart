import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';

class MyIconButton extends StatelessWidget {
  const MyIconButton({
    Key? key,
    required this.icon,
    required this.onTap,
    this.boxShadow,
    this.color = Palette.primary,
    this.iconColor = Palette.onPrimary,
    this.borderRadius,
    this.size = 56,
    this.disabled = false,
  }) : super(key: key);

  final IconData icon;
  final VoidCallback onTap;
  final List<BoxShadow>? boxShadow;
  final Color color;
  final Color iconColor;
  final BorderRadius? borderRadius;
  final double size;
  final bool disabled;

  BorderRadius get _effectiveBorderRadius =>
      borderRadius ?? BorderRadius.circular(16);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: ClipRRect(
        borderRadius: _effectiveBorderRadius,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: color,
            boxShadow: boxShadow,
          ),
          child: Material(
            color: Colors.transparent,
            borderRadius: _effectiveBorderRadius,
            child: IconButton(
              color: iconColor,
              icon: Icon(icon, size: 24),
              onPressed: disabled ? null : onTap,
            ),
          ),
        ),
      ),
    );
  }
}
