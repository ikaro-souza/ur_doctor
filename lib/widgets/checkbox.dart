import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';

const _kAnimationDuration = Duration(milliseconds: 120);
const _kAnimationCurve = Curves.easeInOutQuart;

class CheckBox extends StatefulWidget {
  const CheckBox({
    Key? key,
    required this.label,
    required this.checked,
    required this.onChange,
  }) : super(key: key);

  final String label;
  final bool checked;
  final void Function(bool) onChange;

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildCheckbox(),
          const SizedBox(width: 12),
          Text(
            widget.label,
            style: Theme.of(context).textTheme.caption!.copyWith(
                  height: 1.33,
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ),
          ),
        ],
      ),
    );
  }

  void _onTap() {
    setState(() {
      checked = !checked;
      widget.onChange(!checked);
    });
  }

  Widget _buildCheckbox() {
    return SizedBox(
      height: 20,
      width: 20,
      child: Stack(
        children: [
          Positioned.fill(
            child: _buildOuterBox(),
          ),
          Align(
            alignment: Alignment.center,
            child: _buildInnerBox(),
          ),
        ],
      ),
    );
  }

  Widget _buildOuterBox() {
    final borderColor = checked ? Palette.primary : Palette.outline;
    final borderRadius = BorderRadius.circular(6);

    return DecoratedBox(
      decoration: BoxDecoration(
        color: Palette.surfaceVariant,
        border: Border.all(color: borderColor),
        borderRadius: borderRadius,
      ),
    );
  }

  Widget _buildInnerBox() {
    const double size = 14;
    final borderRadius = BorderRadius.circular(4);
    double opacity = checked ? 1 : 0;

    return AnimatedOpacity(
      opacity: opacity,
      duration: _kAnimationDuration,
      curve: _kAnimationCurve,
      child: Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
          color: Palette.primary,
          borderRadius: borderRadius,
        ),
      ),
    );
  }
}
