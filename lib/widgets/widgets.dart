import 'package:flutter/material.dart';
import 'package:ur_doctor/config/config.dart';

export './app_bar.dart';
export './elevated_button.dart';
export './switch.dart';
export './checkbox.dart';
export './list_item.dart';
export './flat_appbar.dart';
export './icon_button.dart';
export './tabs.dart';

const kHighElevation = [
  BoxShadow(
    offset: Offset(0, 16),
    color: Colors.black12,
    blurRadius: 40,
  )
];

const kUpwardsHighElevation = [
  BoxShadow(
    offset: Offset(0, -16),
    color: Colors.black12,
    blurRadius: 40,
  )
];

const kLowElevation = [
  BoxShadow(
    offset: Offset(0, 12),
    color: Color(0x1A000000),
    blurRadius: 15,
  )
];

final kTextFieldPlaceholder = Container(
  height: 64,
  width: double.infinity,
  decoration: BoxDecoration(
    color: Palette.surfaceVariant,
    borderRadius: BorderRadius.circular(16),
  ),
);

final kChipPlaceholder = Container(
  height: 20,
  width: 64,
  decoration: BoxDecoration(
    color: Palette.surface1,
    borderRadius: BorderRadius.circular(6),
  ),
);
