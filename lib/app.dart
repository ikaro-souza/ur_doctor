import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ur_doctor/config/palette.dart';
import 'package:ur_doctor/features/authentication/screens/screens.dart';
import 'package:ur_doctor/features/common/screens/screens.dart';
import 'package:ur_doctor/features/onboarding/screens/screens.dart';
import 'package:ur_doctor/features/patient/screens/screens.dart';

import 'features/common/state/models/models.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ur Doctor',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.teal)
            .copyWith(secondary: Palette.primary),
        scaffoldBackgroundColor: Palette.background,
        primaryColor: Palette.onBackground,
        dividerColor: Palette.outline,
        chipTheme: ChipThemeData(
          brightness: Brightness.light,
          backgroundColor: Palette.surface2,
          disabledColor: Palette.disabledSurface,
          secondarySelectedColor: Palette.surface2,
          selectedColor: Palette.secondaryContainer,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 12),
          labelPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
          labelStyle: const TextStyle(
            color: Palette.primary,
            height: 1.33,
            letterSpacing: 0.4,
            fontSize: 12,
          ),
          secondaryLabelStyle: const TextStyle(
            color: Palette.onSecondaryContainer,
            height: 1.33,
            letterSpacing: 0.4,
            fontSize: 12,
          ),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Palette.surface2,
          unselectedIconTheme: const IconThemeData(opacity: .75),
          selectedIconTheme: const IconThemeData(opacity: 1, size: 32),
          selectedItemColor: Palette.primary,
          unselectedItemColor: Palette.onSecondaryContainer.withOpacity(.75),
          showSelectedLabels: false,
          showUnselectedLabels: false,
          elevation: 0,
        ),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: Palette.primaryContainer,
          extendedSizeConstraints: const BoxConstraints(
            minHeight: 56,
            minWidth: 123,
            maxHeight: 96,
          ),
          extendedPadding: const EdgeInsets.fromLTRB(16, 16, 20, 16),
          foregroundColor: Palette.onPrimaryContainer,
          largeSizeConstraints: const BoxConstraints(
            minHeight: 96,
            minWidth: 96,
            maxHeight: 96,
            maxWidth: 96,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          sizeConstraints: const BoxConstraints(
            minHeight: 56,
            minWidth: 56,
            maxHeight: 56,
            maxWidth: 56,
          ),
        ),
        dialogTheme: DialogTheme(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
        ),
        textTheme: const TextTheme(
          headline1: TextStyle(
            color: Palette.onBackground,
            height: 1.22,
            fontSize: 36,
            fontWeight: FontWeight.bold,
          ),
          headline2: TextStyle(
            color: Palette.onBackground,
            height: 1.25,
            letterSpacing: 0,
            fontSize: 32,
            fontWeight: FontWeight.bold,
          ),
          headline3: TextStyle(
            color: Palette.onBackground,
            height: 1.286,
            letterSpacing: 0,
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
          headline4: TextStyle(
            color: Palette.onBackground,
            height: 1.33,
            letterSpacing: 0,
            fontSize: 24,
          ),
          bodyText1: TextStyle(
            color: Palette.onBackground,
            height: 1.5,
            letterSpacing: .5,
            fontSize: 16,
          ),
          bodyText2: TextStyle(
            color: Palette.onBackground,
            height: 1.425,
            letterSpacing: .25,
            fontSize: 14,
          ),
          button: TextStyle(
            color: Palette.onPrimary,
            height: 1.5,
            letterSpacing: .1,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
          caption: TextStyle(
            color: Palette.onBackground,
            fontSize: 12,
            height: 1.33,
          ),
        ),
      ),
      initialRoute: PatientOnboarding1.route,
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case PatientOnboarding1.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientOnboarding1(),
            );
          case PatientOnboarding2.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientOnboarding2(),
            );
          case PatientOnboarding3.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientOnboarding3(),
            );

          case PatientSignUpDisplayName.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpDisplayName(),
            );
          case PatientSignUpFullName.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpFullName(),
            );
          case PatientSignUpSex.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpSex(),
            );
          case PatientSignUpTaxId.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpTaxId(),
            );
          case PatientSignUpBirthDate.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpBirthDate(),
            );
          case PatientSignUpPhoneNumber.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpPhoneNumber(),
            );
          case PatientSignUpToken.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpToken(),
            );
          case PatientSignUpEmail.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpEmail(),
            );
          case PatientSignUpCreatePassword.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpCreatePassword(),
            );
          case PatientSignUpConfirmPassword.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpConfirmPassword(),
            );
          case PatientSignUpCompleted.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientSignUpCompleted(),
            );
          case Login.route:
            return CupertinoPageRoute(
              builder: (_) => const Login(),
            );
          case RecoverPassword.route:
            return CupertinoPageRoute(
              builder: (_) => const RecoverPassword(),
            );

          case PatientHome.route:
            return CupertinoPageRoute(
              builder: (_) => const PatientHome(),
            );

          case PatientAppointmentDetails.route:
            return CupertinoPageRoute(
              builder: (_) => PatientAppointmentDetails(
                appointment: settings.arguments! as Appointment,
              ),
            );

          case AppointmentCall.route:
            return CupertinoPageRoute(
              builder: (_) => AppointmentCall(
                appointment: settings.arguments! as Appointment,
              ),
            );
          case AppointmentCallRating.route:
            return CupertinoPageRoute(
              builder: (_) => AppointmentCallRating(
                appointment: settings.arguments! as Appointment,
              ),
            );

          case Chat.route:
            return CupertinoPageRoute(
              builder: (_) => Chat(
                appointment: settings.arguments as Appointment,
              ),
            );

          case ScheduleAppointmentDate.route:
            return CupertinoPageRoute(
              builder: (_) => const ScheduleAppointmentDate(),
            );
          case ScheduleAppointmentSymptoms.route:
            return CupertinoPageRoute(
              builder: (_) => const ScheduleAppointmentSymptoms(),
            );
          case ScheduleAppointmentSymptomsStartDate.route:
            return CupertinoPageRoute(
              builder: (_) => const ScheduleAppointmentSymptomsStartDate(),
            );
          case ScheduleAppointmentDescription.route:
            return CupertinoPageRoute(
              builder: (_) => const ScheduleAppointmentDescription(),
            );
          case ScheduleAppointmentConfirm.route:
            return CupertinoPageRoute(
              builder: (_) => const ScheduleAppointmentConfirm(),
            );
        }
      },
    );
  }
}
